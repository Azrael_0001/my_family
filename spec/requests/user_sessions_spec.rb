require 'spec_helper'

describe "UserSessions" do
  
  before do
    @user = FactoryGirl.create(:user)
  end
  
  describe "GET /login" do
    it "should allow a user to login" do
      visit login_path
      fill_in "user_session_email", :with => @user.email
      fill_in "user_session_password", :with => @user.password
      click_button "login_button"
      current_path.should == user_people_path(@user)
    end
  end
  
  describe "GET /logout" do
    it "should allow a user to logout" do
      visit login_path
      fill_in "user_session_email", :with => @user.email
      fill_in "user_session_password", :with => @user.password
      click_button "login_button"
      current_path.should == user_people_path(@user)
      visit logout_path
      page.should have_content "logged out"
    end
  end
  
end
