# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :user do
    date_of_birth 99.years.ago
    name "Test User"
    email "test@example.com"
    gender "male"
    password "password"
    password_confirmation "password"
  end
end
