# Read about factories at https://github.com/thoughtbot/factory_girl

FactoryGirl.define do
  factory :relationship do
    relationship_type_id 1
    person_id 1
    related_person_id 1
  end
end
