# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)

RelationshipType.create([
  {name:'mother', gender:'female'},
  {name:'father', gender:'male'},
  {name:'step_mother', gender:'female'},
  {name:'step_father', gender:'male'},
  {name:'brother', gender:'male'},
  {name:'sister', gender:'female'},
  {name:'half_brother', gender:'male'},
  {name:'half_sister', gender:'female'},
  {name:'step_brother', gender:'male'},
  {name:'step_sister', gender:'female'},
  {name:'wife', gender:'female'},
  {name:'husband', gender:'male'},
  {name:'ex_wife', gender:'female'},
  {name:'ex_husband', gender:'male'},
  {name:'daughter', gender:'female'},
  {name:'son', gender:'male'}
])