class PeopleController < ApplicationController
  
  before_filter :find_user
  before_filter :require_user
  
  def index
    @person = @user.me
  end
  
  def add
    @person = Person.find_by_id(params[:person_id] || params[:id]) #so it returns nil if it can't be found
    @related_person = @user.people.build(params[:person])
  end
  
  def create 
    @related_person = @user.people.build(params[:person])
    @parent_id = params[:person][:parent_id]
    respond_to do |format|
      if @related_person.save
        format.html { redirect_to( user_person_path(@user, @parent_id)) } #, :notice => "You've created a new person!"
      else
        format.html { render :new, :notice => 'Something went wrong.'}
      end
    end
  end
  
  def show
    @person = Person.find(params[:person_id] || params[:id])
  end
  
end
