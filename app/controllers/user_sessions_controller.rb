class UserSessionsController < ApplicationController
  
  def new
    @user_session = UserSession.new
    respond_to do |format|
      format.html{render :layout => 'pages'}
    end
  end
  
  def create
    @user_session = UserSession.new(params[:user_session])
    respond_to do |format|
      if @user_session.save
        format.html { redirect_to( user_people_path(@user_session.user))} #, :notice => 'Login Successfull')  I think they will know that...
      else
        format.html { render :action => "new" }
      end
    end
  end
  
  def destroy
    @user_session = UserSession.find
    @user_session.destroy
    respond_to do |format|
      format.html { redirect_to( login_path, :notice => 'You have been logged out') }
    end
  end


end