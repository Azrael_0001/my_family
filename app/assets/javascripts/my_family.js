$(document).ready(function(){
    var elems = $('#surrounding_users').find(".person")

    var increase = Math.PI * 2 / elems.length;
    var x = 0, y = 0, angle = 0;

    for (var i = 0; i < elems.length; i++) {
        var elem = elems[i];
        if(elems.length == 1){
        	x = 0;
        } else {
        	x = 240 * Math.cos(angle) + 240;
        }
        // modify to change the radius and position of a circle
        y = 240 * Math.sin(angle) + 240;
        elem.style.position = 'absolute';
        elem.style.left = x + 'px';
        elem.style.top = y + 'px';
        //need to work this part out
        var rot = 90 + (i * (360 / elems.length));
        
        angle += increase;
        console.log(angle);
    }
});