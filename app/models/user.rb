class User < ActiveRecord::Base
  attr_accessible :date_of_birth, :email, :name, :password, :password_confirmation, :gender
  
  acts_as_authentic do |c|
    c.login_field = :email
  end
  
  has_many :people
  
  after_create :create_initial_person
  
  def create_initial_person
    self.people.create(:date_of_birth => self.date_of_birth, :name => self.name, :gender => self.gender.downcase, :is_user => true)
  end
  
  def me
    self.people.find_by_is_user(true)
  end
  
end
